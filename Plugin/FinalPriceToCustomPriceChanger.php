<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Plugin;

class FinalPriceToCustomPriceChanger
{
    private const PRICE_CODES_MAPPER = [
        \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE =>
            \PerspectiveStudio\ProductCustomPriceHandler\Pricing\Price\CustomPrice::PRICE_CODE
    ];

    /**
     * @var \Magento\Framework\App\RequestInterface $request
     */
    private $request;

    /**
     * FinalPriceToCustomPriceChanger constructor.
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->request = $request;
    }

    /**
     * @param \Magento\Catalog\Pricing\Render\FinalPriceBox $subject
     * @param string $priceCode
     * @return string
     */
    public function beforeGetPriceType(
        \Magento\Catalog\Pricing\Render\FinalPriceBox $subject,
        string $priceCode
    ): string {
        if ($this->request->getFullActionName() === 'catalog_category_view'
              && array_key_exists($priceCode, self::PRICE_CODES_MAPPER)
        ) {
            $priceCode = self::PRICE_CODES_MAPPER[$priceCode];
        }

        return $priceCode;
    }
}

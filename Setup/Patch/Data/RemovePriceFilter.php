<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Setup\Patch\Data;

class RemovePriceFilter implements
    \Magento\Framework\Setup\Patch\DataPatchInterface,
    \Magento\Framework\Setup\Patch\PatchRevertableInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    private $logger;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function apply(): self
    {
        return $this->updateValuesForPriceAttribute(false);
    }

    /**
     * @inheritDoc
     */
    public function revert(): void
    {
        $this->updateValuesForPriceAttribute(true);
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [
            \PerspectiveStudio\ProductCustomPriceHandler\Setup\Patch\Data\AddCustomPriceAttribute::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @param bool $allow
     * @return $this
     */
    private function updateValuesForPriceAttribute(bool $allow): self
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $attribute = $eavSetup->getAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price'
        );

        if (!$attribute) {
            return $this;
        }

        $eavSetup->updateAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'price',
            [
                'used_for_sort_by' => $allow,
                'searchable' => $allow,
                'filterable' => $allow,
                'visible_in_advanced_search' => $allow,
                'used_in_product_listing' => $allow
            ]
        );

        return $this;
    }
}

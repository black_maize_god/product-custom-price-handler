<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Setup\Patch\Data;

use PerspectiveStudio\ProductCustomPriceHandler\Ui\DataProvider\Product\Modifier\CustomPrice as CustomPriceUiModifier;

class AddCustomPriceAttribute implements
    \Magento\Framework\Setup\Patch\DataPatchInterface,
    \Magento\Framework\Setup\Patch\PatchRevertableInterface
{
    public const CUSTOM_PRICE_ATTRIBUTE_CODE = 'perspective_studio_custom_price';
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    private $logger;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory  $eavSetupFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function apply(): self
    {
        try {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
            $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                self::CUSTOM_PRICE_ATTRIBUTE_CODE,
                [
                    'group' => 'Advanced Pricing',
                    'label' => 'Custom Price',
                    'input' => 'price',
                    'type' => 'decimal',
                    'required' => false,
                    'backend' => \Magento\Catalog\Model\Product\Attribute\Backend\Price::class,
                    'sort_order' => 2,
                    'default' => null,
                    'user_defined' => false,
                    'apply_to' => implode(',', CustomPriceUiModifier::ALLOWED_TO_MODIFY_PRODUCT_TYPES),
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE,
                    'searchable' => true,
                    'filterable' => true,
                    'visible_in_advanced_search' => true,
                    'used_in_product_listing' => true,
                    'used_for_sort_by' => true,
                    'visible' => true,
                    'visible_on_front' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => true,
                    'is_filterable_in_grid' => true
                ]
            );
        } catch (\Magento\Framework\Exception\LocalizedException | \Zend_Validate_Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function revert(): void
    {
        /** @var \Magento\Eav\Setup\EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->removeAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            self::CUSTOM_PRICE_ATTRIBUTE_CODE
        );
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }
}

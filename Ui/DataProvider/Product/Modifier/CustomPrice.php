<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Ui\DataProvider\Product\Modifier;

use PerspectiveStudio\ProductCustomPriceHandler\Setup\Patch\Data\AddCustomPriceAttribute as CustomPriceAttributeCreator;

class CustomPrice extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier
{
    public const ALLOWED_TO_MODIFY_PRODUCT_TYPES = [
        'simple',
        'virtual',
        'downloadable'
    ];

    /**
     * @var \Magento\Framework\Stdlib\ArrayManager $arrayManager
     */
    private $arrayManager;

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface $locator
     */
    private $locator;

    /**
     * @var \PerspectiveStudio\ProductCustomPriceHandler\Helper\CustomPrice $customPriceHelper
     */
    private $customPriceHelper;

    /**
     * CustomPrice constructor.
     * @param \Magento\Framework\Stdlib\ArrayManager $arrayManager
     * @param \Magento\Catalog\Model\Locator\LocatorInterface $locator
     * @param \PerspectiveStudio\ProductCustomPriceHandler\Helper\CustomPrice $customPriceHelper
     */
    public function __construct(
        \Magento\Framework\Stdlib\ArrayManager $arrayManager,
        \Magento\Catalog\Model\Locator\LocatorInterface $locator,
        \PerspectiveStudio\ProductCustomPriceHandler\Helper\CustomPrice $customPriceHelper
    ) {
        $this->arrayManager = $arrayManager;
        $this->locator = $locator;
        $this->customPriceHelper = $customPriceHelper;
    }

    /**
     * @inheritDoc
     */
    public function modifyData(array $data): array
    {
        $product = $this->locator->getProduct();
        $productId = (int) $product->getId();
        $productData = $data[$productId]['product'];

        if (!array_key_exists(CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE, $productData)
            && in_array($product->getTypeId(), static::ALLOWED_TO_MODIFY_PRODUCT_TYPES)
            && array_key_exists(\Magento\Catalog\Api\Data\BasePriceInterface::PRICE, $productData)
        ) {
            $this->processCustomPrice($data, $productId);
        }

        return $data;
    }

    /**
     * @inheritDoc
     */
    public function modifyMeta(array $meta): array
    {
        return $this->addAllowModifyCheckboxToCustomPriceField($meta);
    }

    /**
     * @param array $data
     * @param int $productId
     * @return void
     */
    protected function processCustomPrice(array &$data, int $productId): void
    {
        $productPrice = (float) $data[$productId]['product'][\Magento\Catalog\Api\Data\BasePriceInterface::PRICE];
        $increaseFactor = $this->customPriceHelper->getIncreasePercent();
        $customPrice = $productPrice + ($productPrice * ($increaseFactor / 100));

        $data[$productId]['product'][CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE] = $customPrice;
    }

    /**
     * @param array $meta
     * @return array
     */
    protected function addAllowModifyCheckboxToCustomPriceField(array $meta): array
    {
        $groupCode = $this->getGroupCodeByField(
            $meta,
            'container_' . CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE
        );

        if (!$groupCode) {
            return $meta;
        }

        $containerPath = $this->arrayManager->findPath(
            'container_' . CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE,
            $meta,
            null,
            'children'
        );
        $fieldPath = $this->arrayManager->findPath(
            CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE,
            $meta,
            null,
            'children'
        );

        $groupConfig = $this->arrayManager->get($containerPath, $meta);
        $fieldConfig = $this->arrayManager->get($fieldPath, $meta);

        $meta = $this->arrayManager->merge($containerPath, $meta, [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => 'container',
                        'componentType' => 'container',
                        'component' => 'Magento_Ui/js/form/components/group',
                        'label' => $groupConfig['arguments']['data']['config']['label'],
                        'breakLine' => false,
                        'sortOrder' => $fieldConfig['arguments']['data']['config']['sortOrder'],
                        'dataScope' => '',
                    ],
                ],
            ],
        ]);

        $meta = $this->arrayManager->merge(
            $containerPath,
            $meta,
            [
                'children' => [
                    CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'dataScope' => CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE,
                                    'additionalClasses' => 'admin__field-small',
                                    // @codingStandardsIgnoreLine
                                    'component' => 'PerspectiveStudio_ProductCustomPriceHandler/js/form/element/text-use-config'
                                ],
                            ],
                        ],
                    ],
                    'use_config_' . CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'dataType' => 'number',
                                    'formElement' => \Magento\Ui\Component\Form\Element\Checkbox::NAME,
                                    'componentType' => \Magento\Ui\Component\Form\Field::NAME,
                                    'description' => __('Allow Modify'),
                                    'dataScope' => 'use_config_'
                                        . CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE,
                                    'valueMap' => [
                                        'false' => 0,
                                        'true' => 1,
                                    ],
                                    'exports' => [
                                        'checked' => '${$.parentName}.'
                                            . CustomPriceAttributeCreator::CUSTOM_PRICE_ATTRIBUTE_CODE
                                            . ':isAvailable',
                                    ]
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        return $meta;
    }
}

<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Helper;

class CustomPrice extends \Magento\Framework\App\Helper\AbstractHelper
{
    private const XML_PATH_CUSTOM_PRICE_INCREASE_PERCENT = 'catalog/products_prices/increase_percent';

    public function getIncreasePercent(): int
    {
        return (int) $this->scopeConfig->getValue(
            self::XML_PATH_CUSTOM_PRICE_INCREASE_PERCENT,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE
        );
    }
}

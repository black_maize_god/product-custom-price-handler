/**
 * @api
 */
define([
    'Magento_Ui/js/form/element/abstract'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            listens: {
                'isAvailable': 'toggleElement',
            }
        },

        /**
         * @inheritdoc
         */
        initObservable: function () {
            return this
                ._super()
                .observe('isAvailable');
        },

        /**
         * Toggle element
         */
        toggleElement: function () {
            this.disabled(!this.isAvailable());
        }
    });
});
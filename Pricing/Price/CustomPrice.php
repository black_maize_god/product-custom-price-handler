<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Pricing\Price;

class CustomPrice extends \Magento\Catalog\Pricing\Price\FinalPrice
{
    const PRICE_CODE = 'perspective_studio_custom_price';

    /**
     * @var \PerspectiveStudio\ProductCustomPriceHandler\Model\ResourceModel\CustomPrice $customPriceResource
     */
    private $customPriceResource;

    /**
     * CustomPrice constructor.
     * @param \Magento\Framework\Pricing\SaleableInterface $saleableItem
     * @param $quantity
     * @param \Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \PerspectiveStudio\ProductCustomPriceHandler\Model\ResourceModel\CustomPrice $customPriceResource
     */
    public function __construct(
        \Magento\Framework\Pricing\SaleableInterface $saleableItem,
        $quantity,
        \Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \PerspectiveStudio\ProductCustomPriceHandler\Model\ResourceModel\CustomPrice $customPriceResource
    ) {
        $this->customPriceResource = $customPriceResource;

        parent::__construct(
            $saleableItem,
            $quantity,
            $calculator,
            $priceCurrency
        );
    }

    /**
     * @inheritDoc
     */
    public function getValue(): float
    {
        $finalPrice = parent::getValue();

        if ($this->product instanceof \Magento\Catalog\Api\Data\ProductInterface) {
            $value = $this->product->getData(
                // @codingStandardsIgnoreLine
                \PerspectiveStudio\ProductCustomPriceHandler\Setup\Patch\Data\AddCustomPriceAttribute::CUSTOM_PRICE_ATTRIBUTE_CODE
            );
        } else {
            $value = $this->customPriceResource->getCustomPriceByProductId(
                (int) $this->product->getId()
            );
        }

        if (!$value) {
            $value = $finalPrice;
        }

        return (float) $value;
    }
}

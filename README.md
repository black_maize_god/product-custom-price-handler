This module adds a custom price attribute to a product
and changes the default logic for the product price.

See link for more information:
https://perspectivestudio.atlassian.net/browse/STD-225
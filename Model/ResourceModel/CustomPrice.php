<?php

declare(strict_types=1);

namespace PerspectiveStudio\ProductCustomPriceHandler\Model\ResourceModel;

class CustomPrice
{
    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute $attributeResource
     */
    private $attributeResource;

    /**
     * @var \Psr\Log\LoggerInterface $logger
     */
    private $logger;

    /**
     * CustomPrice constructor.
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute $attributeResource
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute $attributeResource,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->attributeResource = $attributeResource;
        $this->logger = $logger;
    }

    /**
     * @param int $productId
     * @return float|bool
     */
    public function getCustomPriceByProductId(int $productId)
    {
        $connection = $this->attributeResource->getConnection();
        $result = false;

        $select = $connection
            ->select()
            ->from($this->attributeResource->getTable('catalog_product_entity_decimal'), 'value')
            ->where(new \Zend_Db_Expr(
                "entity_id = $productId and attribute_id in ("
                 . $connection
                    ->select()
                    ->from($this->attributeResource->getTable('eav_attribute'), 'attribute_id')
                    ->where(
                        'attribute_code = "'
                            . \PerspectiveStudio\ProductCustomPriceHandler\Pricing\Price\CustomPrice::PRICE_CODE . '"'
                    )
                . ')'
            ));

        try {
            $result = $connection->query($select)->fetchColumn(0);
        } catch (\Zend_Db_Statement_Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $result ? (float) $result : $result;
    }
}
